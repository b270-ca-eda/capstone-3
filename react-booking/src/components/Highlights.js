import { Row, Col, Card } from 'react-bootstrap';
import Video from './pics/vid.mp4'
export default function Highlights() {
	return (
		<Row style={{marginTop:"12em", marginBottom:"12em"}}>
		<Col xs={12} md={2}>
				
			</Col>
			<Col xs={12} md={8}>
				<Card className="cardHighlight p-3" style={{paddingTop:"2em"}}>
				    <Card.Body>
				    <video src={Video} controls style={{height:"100%", width:"100%"}}></video>
				        <Card.Title>Authentic Japanes Taste</Card.Title>		        
				        <Card.Text>
				         You can visit our store and see us cook your takoyaki right in front of you and experience authentic Japanese in every bite.
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={2}>
				
			</Col>
		</Row>

	)
}