import { Row, Col, Card } from 'react-bootstrap';


export default function Comments() {
	return (
		<Row style={{marginBottom:"12em"}}>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{paddingTop:"2em"}}>
					<Card.Img variant="top" src="./pics/bg7.png"/>
				    <Card.Body>
				    	
				        <Card.Title style={{paddingTop:"2em", paddingBottom: "2em"}}>5.0 stars  </Card.Title>		        
				        <Card.Text style={{paddingBottom: "1em"}}>
				          Definitely the best in the city. Served fresh and hot every SINGLE time and tastes just like the takoyaki I've tasted in Japan! 
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{paddingTop:"2em"}}>
				    <Card.Img variant="top" src="./pics/bg5.png"/>
				    <Card.Body>
				        <Card.Title style={{paddingTop:"2em", paddingBottom: "2em"}}>5.0 stars</Card.Title>		        
				        <Card.Text tyle={{paddingBottom: "4em"}}>
				         Yummy! Yummy! Yummy! Should I say more???
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{paddingTop:"2em"}}>
				  <Card.Img variant="top" src="./pics/bg2.png"/>
				    <Card.Body>
				        <Card.Title style={{paddingTop:"2em", paddingBottom: "2em"}}>5.0 stars</Card.Title>		        
				        <Card.Text tyle={{paddingBottom: "4em"}}>
				          People just can't stop debating which takoyaki is the best. I think we now have the winner. The clear winner. It's asclear as the day that Bi-maksu takoyaki is way ahead in this race. Taste, customer service, chat responsiveness, and did I say TASTE??
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}