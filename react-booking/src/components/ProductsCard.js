import { useState, useEffect, useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function ProductsCard({productsProp}) {

	console.log(productsProp);

	// Deconstructs the course properties into their own variables
	const {_id, name, description, price } = productsProp;
    const [showAdd, setShowAdd] = useState(false);
    const { user } = useContext(UserContext);
    return(
    (user.isAdmin)
    ?
    <>
    <Row style={{}}>
    <Col md={3} style={{background:"./pics/bg3.png}}"}}>
    </Col>
    <Col  md={6} xs={12}>
       <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                  <Button variant="primary" as={Link} to={`/adminDashboard`}>Edit</Button>
                 
                
                
            </Card.Body>
        </Card>
    </Col>
    <Col md={3}>
    </Col>
    </Row>
    </>
    :
    <>
    <Row style={{}}>
    <Col md={3} style={{background:"./pics/bg3.png}}"}}>
    </Col>
    <Col  md={6} xs={12}>
       <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/products/${_id}`}>+</Button>
                 
                
            </Card.Body>
        </Card>
    </Col>
    <Col md={3}>
    </Col>
    </Row>
    </>
    )
}

