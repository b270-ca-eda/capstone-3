import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import styles from './index.css';
import background from "./pics/bg6.png";

import Carousel from 'react-bootstrap/Carousel';

function Banner({data}) {

const {title, content, destination, label} = data;

  return (

  <Row>

			{/*<Col className="my-10em text-center container-fluid bg-image
			" style={{outline: "2px solid white", margin:"8em", padding:"3em", position:"relative"}}>
				<h1>{title}</h1>
    			<p>{content}</p>
				<Button as = {Link} to={destination} variant="danger" style={{background:"none", outline:"2px solid white"}}>{label}</Button>
			</Col>*/}
		
  <Col xs={12}>
    <Carousel fade>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="./pics/bg9.png"
          alt="First slide"
        />
     
        <Carousel.Caption style={{transform: "translateY(-50%)", top:"50%", bottom:"initial"}}>
	        <Row style={{outline: "2px solid white" , padding:"3em"}}>
					<Col xs={12} className="my-10em text-center container-fluid bg-image
					">
						<h1>{title}</h1>
		    			<p>{content}</p>
						<Button as = {Link} to={destination} variant="danger" style={{background:"none", outline:"2px solid white"}}>{label}</Button>
					</Col>
				</Row>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="./pics/bg8.png"
          alt="Second slide"
        />

      
        <Carousel.Caption style={{transform: "translateY(-50%)", top:"50%", bottom:"initial"}}>
		      <Row style={{outline: "2px solid white" , padding:"3em"}}>
					<Col className="my-10em text-center container-fluid bg-image
					">
						<h1>{title}</h1>
		    			<p>{content}</p>
						<Button as = {Link} to={destination} variant="danger" style={{background:"none", outline:"2px solid white"}}>{label}</Button>
					</Col>
				</Row>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="./pics/bg6.png"
          alt="Third slide"
        />


        <Carousel.Caption style={{transform: "translateY(-50%)", top:"50%", bottom:"initial"}}>
		      <Row style={{outline: "2px solid white" , padding:"3em"}}>
					<Col xs={12} className="my-10em text-center container-fluid bg-image
					">
						<h1>{title}</h1>
		    			<p>{content}</p>
						<Button as = {Link} to={destination} variant="danger" style={{background:"none", outline:"2px solid white"}}>{label}</Button>
					</Col>
				</Row>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  </Col>
  </Row>


  );
}

export default Banner;

/*export default function Banner({data}) {

	console.log(data)
	const {title, content, destination, label} = data;
	const bannerStyle = 
		{backgroundImage: `url(${background})`, 
		padding:"20em", 
		position: "relative",
		backgroundRepeat: "no-repeat",
		backgroundSize:"100%",
		outline:"1px solid black",
		color:"white" }

		const  buttonStyle = {
			padding:"15px",
			outline: "2px solid white",
			background: "none"

		}
	


	return (
		<Row style={bannerStyle}>
			<Col className="p-5 text-center container-fluid bg-image
			">
				<h1>{title}</h1>
    			<p>{content}</p>
				<Button as = {Link} to={destination} style={buttonStyle}>{label}</Button>
			</Col>
		</Row>
	)
}*/