
import React from 'react';

import { SocialIcon } from 'react-social-icons';


export default function Contact() {

	return(
	<>
	<div class="row" style={{marginTop:"5em"}}>
		<div style={{textAlign:"center"}}>
			<SocialIcon url="https://facebook.com/senjade" />, 
			<SocialIcon url="https://instagram.com" />
		</div>
		<div class="col-md-2 my-4">
		</div>
          <div class="col-md-4 my-4">
              <h4 class="text-center my-4">Visit our Store</h4>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3703.630696329391!2d123.75332718682338!3d10.205484297787619!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a97853b4701ac5%3A0x817fd75dc8393390!2sOceanside%20Village!5e0!3m2!1sen!2sph!4v1679627854484!5m2!1sen!2sph" style={{margin:"1em", height:"100%", width: "100%", outline:"2px solid black"}}></iframe>

           
          </div>
          <div class="col-md-1 my-4">
		</div>
          <div class="col-md-3 my-4">
              <h4 class="text-center my-4">For enquiries, send us a message</h4>

              
             <form class="needs-validation" novalidate>
                  <div class="form-group">
                      <label for="form1">Full Name</label>
                      <input type="text" class="form-control" name="form1" id="form1" required/>
                    
                  </div>
                  <div class="form-group">
                      <label for="form2">Email Address</label>
                      <input type="email" class="form-control" name="form2" id="form2" required="required" />
                      
                  </div>
                  <div class="form-group">
                      <label for="form3">Message</label>
                      <textarea class="form-control" name="form3" id="form3"required></textarea>
                    
                  </div>
                  
              
                 <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                   Send
                 </button>

                 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                   <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Success!</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                         </button>
                       </div>
                       <div class="modal-body">
                        You have succesfully sent an email
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                   
                       </div>
                     </div>
                   </div>
                 </div>

              </form>
          </div>
          <div class="col-md-2 my-4">
		</div>
      </div>

      
	

	</>
	
       )
	
}