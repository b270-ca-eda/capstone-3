import { Row, Col, Card } from 'react-bootstrap';
import Contact from '../components/Contact';
import { Fragment } from 'react';

export default function Faq() {
	return(
	<Fragment>
		<Row>
			<Col md={3} style={{background: "red"}}>
				
			</Col>

			<Col xs={12} md={6} >
				<h1 style={{marginBottom:"1.5em", marginTop:"1em"}}>Frequently Asked Questions</h1>
				
				<div>
					<h5>Q: What is the store operating hours?</h5>
					<p>A: We are open from 11 am till 10 pm daily.</p>
				</div>
				<div>
					<h5>Q: How much is the delivery fee?</h5>
					<p>A: The fee depends on your location. The more distant the proximity, the larger the fee is. The fee starts at P30.</p>
				</div>
				<div>
					<h5>Q: Are your ingredients always fresh?</h5>
					<p>A: The freshness of our products is one of the many things that we can boast about Bi-maksu Takoyaki.</p>
				</div>
				<div>
					<h5>Q: When is your schedule for deliveries?</h5>
					<p>A: Your orders are immediately delivered right after preparing so you can enjoy your takoyaki hot.</p>
				</div>
				<div>
					<h5>Q: Are you a franchising business?</h5>
					<p>A: You currently can't franchise Bi-maksu takokyako but we are currently restructuring and plans are already in place to expand the business. Stay tuned for more updates.</p>
				</div>
				<div>
					<h5>Q: Are you open for reselling your products?</h5>
					<p>A: You can send us an email to discuss about reselling.</p>
				</div>
				<div style={{marginBottom: "1.5em"}}>
					<h5>Q: Do you have a physical store?</h5>
					<p>A: Our store is located in the Gate are of the Oceansie Village in the city of Naga, Cebu.</p>
				</div>
				
					
				
			</Col>
			<Col md={3} style={{background: "red"}}>
			</Col>
		</Row>
		<Contact />
	</Fragment>
	)
}